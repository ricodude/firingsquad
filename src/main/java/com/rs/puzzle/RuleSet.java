package com.rs.puzzle;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class RuleSet {

    private Map<String, String> rules;

    RuleSet(Path rulesFile) {
        rules = new HashMap();
        try (BufferedReader reader = Files.newBufferedReader(rulesFile)) {
            // Read in & process each line from the rules file
            String line;
            while ((line = reader.readLine()) != null) {
                // Separate LHS & RHS of rule
                String[] ruleStrings = StringUtils.split(line, ':');
                // Check the rule has been well-formed
                if (ruleStrings.length != 2 ||
                        ruleStrings[0].length() != 3 ||
                        ruleStrings[1].length() != 1) {
                    throw new RuntimeException("Rule \"" + line + "\" is badly formed");
                }
                rules.put(ruleStrings[0], ruleStrings[1]);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public char[] applyRules(char[] state) {
        char[] newState = new char[state.length];
        newState[0] = '|';
        newState[newState.length - 1] = '|';
        int len = state.length - 2;
        for (int i = 1; i <= len ; i++) {
            String key = new String(Arrays.copyOfRange(state, i - 1, i + 2));
            String newCharString = rules.get(key);
            char newChar;
            if (newCharString != null) { newChar = newCharString.toCharArray()[0]; }
            else { newChar = '#'; }
            newState[i] = newChar;
        }
        return newState;
    }
}
