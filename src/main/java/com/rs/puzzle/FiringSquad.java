package com.rs.puzzle;

import org.apache.commons.lang3.StringUtils;

public class FiringSquad {

    private char[] state;
    private RuleSet rules;

    FiringSquad(int len, RuleSet rules) {
        String stateString = "|" + StringUtils.repeat("_", len) + "|";
        state = stateString.toCharArray();
        this.rules = rules;
    }

    public void go(int maxSteps) {
        state[1] = 'G';
        int step = 1;
        while (step <= maxSteps) {
            String stateString = new String(state);
            System.out.format("%4d" + stateString + "\n", step);
            state = rules.applyRules(state);
            step++;
        }
        System.out.println();
    }
}
