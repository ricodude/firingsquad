package com.rs.puzzle;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) {
        /*
        arg 0: start number for squad length to test - e.g. 3
        arg 1: end number for squad length to test - e.g. 100
        arg 2: length multiple for number of steps to force termination - e.g. 3
        arg 3: file specifying rules - e.g. "/Users/richard/Desktop/firing_squad_rules"
        */
        if (args.length != 4) {
            throw new RuntimeException("** ERROR - wrong no. of arguments (should be 4)");
        }

        int startNum = Integer.parseInt(args[0]);
        int endNum = Integer.parseInt(args[1]);
        int stopMultiple = Integer.parseInt(args[2]);
        Path rulesFile = Paths.get(args[3]);

        // Initialise rule set with rules from file
        RuleSet rules = new RuleSet(rulesFile);

        // Try rules for each length of squad
        for (int len = startNum; len <= endNum ; len++) {
            // Create a new firing squad
            FiringSquad squad = new FiringSquad(len, rules);
            squad.go(len * stopMultiple);
        }
    }
}
